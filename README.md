# Gitlab CI/CD NPM Example

This project is an example for how to publish an npm package to the Gitlab registry via a CI/CD pipeline.

## To install

```bash
echo @djdietrick:registry=https://gitlab.com/api/v4/projects/<project_id>/packages/npm/ >> .npmrc
npm install --save @djdietrick/gitlab-cicd-npm-example
```